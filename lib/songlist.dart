import 'dart:io';
import 'package:audio_picker/audio_picker.dart';
import 'package:callmusic/main.dart';
import 'package:flutter/material.dart';

class SongList extends StatefulWidget {
  List<String> list = List();

  SongList(this.list);

  @override
  _SongListState createState() => _SongListState(list);
}

class _SongListState extends State<SongList> {
  List<String> list = List();
  var _absolutePathOfAudio;

  void gtfile() async {
    var path = await AudioPicker.pickAudio();

    setState(() {
      list.add(path);
      print(path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Song List"),
      ),
      body: Column(
        children: [
          ListView.builder(
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => MyHomePage(list[index], list),
                  ));
                },
                child: Container(
                  margin: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 30),
                        child: Column(
                          children: [
                            Container(
                              child: Text(
                                list[index],
                                style: TextStyle(fontSize: 10),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            list.removeAt(index);
                          });
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.blueAccent)),
                            margin:
                                EdgeInsets.only(right: 30, top: 10, bottom: 10),
                            child: Padding(
                              padding: const EdgeInsets.all(4),
                              child: Text(
                                "Delete",
                                style: TextStyle(fontSize: 10),
                              ),
                            )),
                      )
                    ],
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent)),
                ),
              );
            },
            itemCount: list.length,
          ),
          RaisedButton(
            child: Text("Add song"),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
                side: BorderSide(color: Colors.red)),
            onPressed: () {
              gtfile();
            },
          )
        ],
      ),
    );
  }

  _SongListState(this.list);
}
