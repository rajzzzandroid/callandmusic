import 'dart:async';
import 'dart:developer';

// ignore: avoid_web_libraries_in_flutter
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:callmusic/songlist.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:seekbar/seekbar.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:callmusic/config.dart' as config;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    List<String> list = List();

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage("", list),
    );
  }
}

class MyHomePage extends StatefulWidget {
  String path;
  List<String> list = List();

  @override
  State<StatefulWidget> createState() => _State(path, list);

  MyHomePage(this.path, this.list);
}

RtcEngine _engine;

class _State extends State<MyHomePage> {
  String path = "";

  List<String> list = List();

  Map<int, bool> _users = Map();
  bool play = false;
  bool taking = false;
  int duration = 0;

  double _value = 0.0;
  double volumechange = 0.0;

  Timer _progressTimer;
  Timer _secondProgressTimer;

  bool _done = false;

  @override
  void initState() {
    _resumeProgressTimer();
    _secondProgressTimer =
        Timer.periodic(const Duration(milliseconds: 10), (_) {
      setState(() {});
    });
    super.initState();
    _initAgoraRtcEngine();
  }

  _resumeProgressTimer() {
    _progressTimer = Timer.periodic(const Duration(milliseconds: 10), (_) {
      setState(() {
        _engine
            .getAudioMixingCurrentPosition()
            .then((value) => {_value = ((value / 1000) / 60)});
        if (_value >= 1) {
          _progressTimer.cancel();
          _done = true;
        }
      });
    });
  }

  @override
  void dispose() {
    _progressTimer?.cancel();
    _secondProgressTimer?.cancel();
    _engine.leaveChannel();
    _engine.disableAudio();
    _engine.stopAudioMixing();
    _engine.enableLocalAudio(false);
  }

  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.create("ad98bea8abe34b94a57310e8cb19886a");
    await _engine.enableAudio();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(ClientRole.Broadcaster);
    events();
    _joinChannel();
  }

  _joinChannel() async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      await Permission.microphone.request();
    }
    await _engine?.joinChannel(config.token, config.channelId, null,
        DateTime.now().millisecondsSinceEpoch);
    _users[DateTime.now().millisecondsSinceEpoch] = false;
    _engine?.enableLocalAudio(true);
    /*
    _engine.enableAudioVolumeIndication(200, 3, true);
    print(_engine.isSpeakerphoneEnabled());*/
  }

  showAlertDialog(BuildContext context) {
    Widget slider = Slider(
      onChanged: (value) {
        setState(() {
          volumechange = value;
          _engine.adjustAudioMixingPlayoutVolume(volumechange.toInt());
          _engine.adjustAudioMixingPublishVolume(volumechange.toInt());

          print(value);
        });
      },
      max: 100,
      min: 0,
      value: volumechange,
    );

    AlertDialog alert = AlertDialog(
      title: Text("volume Alert"),
      actions: [
        slider,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void events() {
    _engine.setEventHandler(RtcEngineEventHandler(
      audioVolumeIndication: (speakers, totalVolume) {},
      error: (err) {
        print('ERROR:$err');
      },
      userJoined: (uid, elapsed) {
        setState(() {
          final info = 'onUserjoined: $elapsed, uid: $uid';
          _users[uid] = false;
          print(info);
        });
      },
      joinChannelSuccess: (channel, uid, elapsed) {
        setState(() {
          final info = 'onJoinChannel: $channel, uid: $uid';
          print(info);
        });
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Call Audio with music"),
        ),
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(left: 30, top: 30),
                child: GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 10,
                  ),
                  itemBuilder: (context, index) {
                    return Container(
                        child: Image(
                      image: AssetImage('assets/images/m.png'),
                      fit: BoxFit.contain,
                      height: 50,
                      width: 50,
                    ));
                  },
                  itemCount: _users.length,
                ),
              ),
              Container(
                height: 150,
                width: double.infinity,
                margin: EdgeInsets.only(bottom: 50, left: 30, right: 30),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                child: Container(
                    child: Column(
                  children: [
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        alignment: Alignment.center,
                        color: Colors.black87,
                        child: Slider(
                          onChanged: (value) {
                            setState(() {
                              _value = value;
                            });
                          },
                          max: (duration / 1000) / 60,
                          min: 0,
                          value: _value,
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(_value.toString()),
                        Text(((duration / 1000) / 60).toString()),
                      ],
                    ),
                    Container(
                      child: Text(
                        path,
                        style: TextStyle(fontSize: 10),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            showAlertDialog(context);
                          },
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20, right: 20, bottom: 10),
                              child: Image(
                                image: AssetImage('assets/images/v.png'),
                                fit: BoxFit.contain,
                                height: 50,
                                width: 50,
                              )),
                        ),
                        GestureDetector(
                          onTap: () {
                            if (!play) {
                              setState(() {
                                _engine.startAudioMixing(
                                    path, false, false, -1);
                                _engine.adjustAudioMixingVolume(50);
                                play = true;
                                _engine
                                    .getAudioMixingDuration()
                                    .then((value) => duration = value);
                              });
                            } else {
                              setState(() {
                                _engine.pauseAudioMixing();
                                play = false;
                              });
                            }
                          },
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20, right: 20, bottom: 10),
                              child: Image(
                                image: play
                                    ? AssetImage('assets/images/pause.png')
                                    : AssetImage('assets/images/pl.png'),
                                fit: BoxFit.contain,
                                height: 50,
                                width: 50,
                              )),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                              builder: (context) => SongList(list),
                            ));
                          },
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: 20, right: 20, bottom: 10),
                              child: Image(
                                image: AssetImage('assets/images/menuss.png'),
                                fit: BoxFit.contain,
                                height: 50,
                                width: 50,
                              )),
                        ),
                      ],
                    )
                  ],
                )),
              )
            ],
          ),
        ));
  }

  _State(this.path, this.list);
}
